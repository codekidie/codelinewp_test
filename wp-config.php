<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'codelinewptest');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '-6#MYX@/UazoYV7C;$/w-miebdI|JA9x[-f|E&fqU#DJXY!Ie6]%G:CF~a%/fL_H');
define('SECURE_AUTH_KEY',  'vJ^B@-FPV&U/LZS,tsx8DPhs8u2gQK5YvUuJ4!55G{eLa|~!ReGT[|wP@)bLoy~~');
define('LOGGED_IN_KEY',    '0l:l)fc+m^:u<$^}qRXq09O2ED5tPkWOwES]YD+/7vUR3(pd#v+w?^*YP8zJHk/c');
define('NONCE_KEY',        '=e:(%A^1C.X8~hXV6?tr;GUV,Y>@G{sAaH9:_BTF4O2V mz-1*}?CpG(AaC:=qR>');
define('AUTH_SALT',        '+&r$Fv}?~qima^V=uAfOK)rN]_>~Rim0~>T%BNKHzQ+3F&1V<MJ-}W}J9X,aki%s');
define('SECURE_AUTH_SALT', 'pGhDxEdcg>U+SCH@c)(@+ d9wExz-EHf%,i9k{{l(hg.]N8++S[7[6#R1h?kS#pv');
define('LOGGED_IN_SALT',   '1^z>N7g*D0n.JUdlww&eOoJc}RXdH-P%(M%%%tTfmz[G(5 o.~=9O]ARxgVtaX,_');
define('NONCE_SALT',       'bR@]Gwqu%8{h$CHAOX_n&.idFyER:|k,Lj:4tnzypm;JM`qJj,:u Ok4Xj/:AFj[');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
