<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

// BEGIN ENQUEUE PARENT ACTION
// AUTO GENERATED - Do not modify or remove comment markers above or below:

if ( !function_exists( 'chld_thm_cfg_parent_css' ) ):
    function chld_thm_cfg_parent_css() {
        wp_enqueue_style( 'chld_thm_cfg_parent', trailingslashit( get_template_directory_uri() ) . 'style.css', array( 'unite-bootstrap','unite-icons' ) );
    }
endif;
add_action( 'wp_enqueue_scripts', 'chld_thm_cfg_parent_css', 10 );

// END ENQUEUE PARENT ACTION


// Edyls Short codes

function getFirstFiveFilms( ) {
   // the query
   $the_query = new WP_Query( array(
     'category_name' => 'films',
      'posts_per_page' => 5,
   )); 

if ( $the_query->have_posts() ) : 
   while ( $the_query->have_posts() ) : $the_query->the_post(); 
     the_title();
     the_excerpt(); 
   endwhile; 
   wp_reset_postdata(); 

else : 
  echo '<p>'.__('No Films Found');
endif; 
}
add_shortcode( 'ed_five_films', 'getFirstFiveFilms' );

add_filter( 'widget_text', 'do_shortcode' );